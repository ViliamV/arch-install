#!/usr/bin/env bash

STEPS=(
  "change_origin_url"
  "add_user_to_groups"
  "install_paru"
  "import_gpg"
  "enable_ssh"
  "setup_dotfiles"
  "install_packages"
  "enable_ufw"
)
touch completed.txt
mapfile -t COMPLETED < completed.txt

for step in "${STEPS[@]}"; do
  if [[ ! " ${COMPLETED[@]} " =~ " ${step} " ]]; then
    read -er -i "Y" -n1 -p "${step}? [Y/n]: " cont
    if [[ ${cont} =~ y|Y ]]; then
      if bash -x "./scripts/${step}.sh"; then
        echo "$step" >> completed.txt
      else
        echo "Failed to complete the step - exitting"
        exit 1
      fi
    fi
  fi
done
echo "All done!"
