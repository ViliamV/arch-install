#!/usr/bin/env bash

git clone https://aur.archlinux.org/paru.git
cd paru || exit
makepkg -si
cd ..
rm -rf paru
paru -S paru-bin
