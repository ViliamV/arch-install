#!/usr/bin/env bash

paru -S --needed udisks2

echo "GPG:"
echo "On another pc mount USB drive, cd there and run:"
echo "gpg --export-secret-keys --armor --output deleteme.asc <USER_ID>"
echo "gpg --export-ownertrust > ownertrust.txt"
echo

lsblk
read -re -p "USB DEVICE name (e.g. /dev/sda1) ?: " dev
[ -z "$dev" ] && exit 1
udisksctl mount -b "$dev" || exit 1
read -re -p "USB MOUNTPOINT?: " mountpoint
[ -z "$mountpoint" ] && exit 1

gpg --import "$mountpoint/deleteme.asc" && rm "$mountpoint/deleteme.asc"
gpg --import-ownertrust "$mountpoint/ownertrust.txt" && rm "$mountpoint/ownertrust.txt"
rm -f "$mountpoint/deleteme.asc"
udisksctl unmount -b "$dev" || exit 1
