#!/bin/bash

if [ "$(whoami)" != "root" ]; then
  sudo su -c "bash $0"
  exit $?
fi

echo "-> UFW"
ufw default deny
echo "Allow everything only from local quantlane network"
ufw allow from 10.107.20.0/24
ufw enable
systemctl enable --now ufw.service
ufw logging off
ufw status | grep Status
