#!/usr/bin/env bash

declare -a PACKAGES
while read -r line; do
  [[ -n "$line" ]] && [[ ${line:0:1} != \# ]] && PACKAGES+=("$line")
done < packages.txt

paru -S --needed "${PACKAGES[@]}"

