#!/bin/bash

if [ "$(whoami)" != "root" ]; then
  sudo su -c "bash $0 $(whoami)"
  exit $?
fi

echo "-> Adding user $1 to groups lp,input"
gpasswd -a "$1" lp
gpasswd -a "$1" input
# echo "-> Enabling sudo commands without password"
# sed -i -r 's/# %wheel ALL=\(ALL\) NOPASSWD: ALL/%wheel ALL=\(ALL\) NOPASSWD: ALL/' /etc/sudoers
