#!/usr/bin/env bash

paru -S --needed chezmoi
read -re -i "Y" -p "Are you in TTY (no GUI)?: " tty
[[ $tty =~ y|Y ]] && export GPG_TTY=$(tty)
chezmoi --source "$HOME/dotfiles" init --apply https://gitlab.com/ViliamV/chezmoi-dotfiles
chezmoi git remote set-url origin git@gitlab.com:ViliamV/chezmoi-dotfiles.git
